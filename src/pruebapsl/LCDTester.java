/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebapsl;

import java.util.Scanner;

/**
 *
 * @author Jairo Rios
 */
public class LCDTester {

    private static final String CADENA_FINAL = "0,0";

    public static void main(String[] args) {
        ImpresorLCD impresorLCD = new ImpresorLCD();
        int espacioDig;
        try {
            try (Scanner lector = new Scanner(System.in)) {
                System.out.print("Espacio entre Digitos (0 a 5): ");
                String comando = lector.next();
                // Valida si es un numero
                if (ImpresorLCD.isNumeric(comando)) {
                    espacioDig = Integer.parseInt(comando);
                    // se valida que el espaciado este entre 0 y 5
                    if (espacioDig < 0 || espacioDig > 5) {
                        throw new IllegalArgumentException("El espacio entre "
                                + "digitos debe estar entre 0 y 5");
                    }
                } else {
                    throw new IllegalArgumentException("Cadena " + comando
                            + " no es un entero");
                }
                System.out.print("Entrada: ");
                do {
                    comando = lector.next();
                    if (!comando.equalsIgnoreCase(CADENA_FINAL)) {
                        impresorLCD.procesar(comando, espacioDig);
                    }
                } while (!comando.equalsIgnoreCase(CADENA_FINAL));
            }
        } catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }
}
