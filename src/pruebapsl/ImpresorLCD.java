/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebapsl;

import java.util.ArrayList;

/**
 *
 * @author Jairo Rios
 */
public class ImpresorLCD {

    // Puntos fijos
    private final int[] pf1;
    private final int[] pf2;
    private final int[] pf3;
    private final int[] pf4;
    private final int[] pf5;
    private String[][] matrizImpr;
    private int size;

    private static final String CARACTER_VERTICAL = "|";
    private static final String CARACTER_HORIZONTAL = "-";
    private static final String POSICION_X = "X";
    private static final String POSICION_Y = "Y";

    public ImpresorLCD() {
        // Inicializa variables
        this.pf1 = new int[2];
        this.pf2 = new int[2];
        this.pf3 = new int[2];
        this.pf4 = new int[2];
        this.pf5 = new int[2];
    }

    /**
     *
     * Metodo encargado de añadir una linea a la matriz de Impresion
     *
     * @param punto Punto Pivote
     * @param posFija Posicion Fija
     * @param caracter Caracter Segmento
     */
    private void adicionarLinea(int[] punto, String posFija, String caracter) {
        if (posFija.equalsIgnoreCase(POSICION_X)) {
            for (int y = 1; y <= this.size; y++) {
                int valor = punto[1] + y;
                this.matrizImpr[punto[0]][valor] = caracter;
            }
        } else {
            for (int i = 1; i <= this.size; i++) {
                int valor = punto[0] + i;
                this.matrizImpr[valor][punto[1]] = caracter;
            }
        }
    }

    /**
     *
     * Metodo encargado de un segmento a la matriz de Impresion
     *
     * @param segmento Segmento a adicionar
     */
    private void adicionarSegmento(int segmento) {
        switch (segmento) {
            case 1:
                adicionarLinea(this.pf1, POSICION_Y, CARACTER_VERTICAL);
                break;
            case 2:
                adicionarLinea(this.pf2, POSICION_Y, CARACTER_VERTICAL);
                break;
            case 3:
                adicionarLinea(this.pf5, POSICION_Y, CARACTER_VERTICAL);
                break;
            case 4:
                adicionarLinea(this.pf4, POSICION_Y, CARACTER_VERTICAL);
                break;
            case 5:
                adicionarLinea(this.pf1, POSICION_X, CARACTER_HORIZONTAL);
                break;
            case 6:
                adicionarLinea(this.pf2, POSICION_X, CARACTER_HORIZONTAL);
                break;
            case 7:
                adicionarLinea(this.pf3, POSICION_X, CARACTER_HORIZONTAL);
                break;
            default:
                break;
        }
    }

    /**
     *
     * Metodo encargado de definir los segmentos que componen un digito y a
     * partir de los segmentos adicionar la representacion del digito a la
     * matriz
     *
     * @param numero Digito
     */
    private void adicionarDigito(int numero) {

        // Establece los segmentos de cada numero
        ArrayList<Integer> segList = new ArrayList<>();

        switch (numero) {
            case 1:
                segList.add(3);
                segList.add(4);
                break;
            case 2:
                segList.add(5);
                segList.add(3);
                segList.add(6);
                segList.add(2);
                segList.add(7);
                break;
            case 3:
                segList.add(5);
                segList.add(3);
                segList.add(6);
                segList.add(4);
                segList.add(7);
                break;
            case 4:
                segList.add(1);
                segList.add(6);
                segList.add(3);
                segList.add(4);
                break;
            case 5:
                segList.add(5);
                segList.add(1);
                segList.add(6);
                segList.add(4);
                segList.add(7);
                break;
            case 6:
                segList.add(5);
                segList.add(1);
                segList.add(6);
                segList.add(2);
                segList.add(7);
                segList.add(4);
                break;
            case 7:
                segList.add(5);
                segList.add(3);
                segList.add(4);
                break;
            case 8:
                segList.add(1);
                segList.add(2);
                segList.add(3);
                segList.add(4);
                segList.add(5);
                segList.add(6);
                segList.add(7);
                break;
            case 9:
                segList.add(1);
                segList.add(3);
                segList.add(4);
                segList.add(5);
                segList.add(6);
                segList.add(7);
                break;
            case 0:
                segList.add(1);
                segList.add(2);
                segList.add(3);
                segList.add(4);
                segList.add(5);
                segList.add(7);
                break;
            default:
                break;
        }

        for (int i = 0; i < segList.size(); i++) {
            adicionarSegmento(segList.get(i));
        }
    }

    /**
     *
     * Metodo encargado de imprimir un numero
     *
     * @param size Tamaño Segmento Digitos
     * @param numeroImp Numero a Imprimir
     * @param espacio Espacio Entre digitos
     */
    private void imprimirNumero(int size, String numeroImp, int espacio) {
        // Calcula el numero de columna de cada digito
        int columDig = size + 2;
        // Calcula el numero de filasDig
        int filasDig = (2 * size) + 3;

        // Calcula el total de columnas de la matriz en la que se almacenaran los digitos
        int totalColum = (columDig * numeroImp.length())
                + (espacio * numeroImp.length());

        // crea matriz para almacenar los numero a imprimir
        this.matrizImpr = new String[filasDig][totalColum];
        this.size = size;

        // crea el arreglo de digitos
        char[] digitos = numeroImp.toCharArray();

        // Inicializa matriz
        for (int i = 0; i < filasDig; i++) {
            for (int j = 0; j < totalColum; j++) {
                this.matrizImpr[i][j] = " ";
            }
        }
        int pivotX = 0;
        for (char digito : digitos) {
            //Valida que el caracter sea un digito
            if (!Character.isDigit(digito)) {
                throw new IllegalArgumentException("Caracter " + digito
                        + " no es un digito");
            }
            
            //Calcula puntos fijos
            this.pf1[0] = 0;
            this.pf1[1] = 0 + pivotX;

            this.pf2[0] = (filasDig / 2);
            this.pf2[1] = 0 + pivotX;

            this.pf3[0] = (filasDig - 1);
            this.pf3[1] = 0 + pivotX;

            this.pf4[0] = (columDig - 1);
            this.pf4[1] = (filasDig / 2) + pivotX;

            this.pf5[0] = 0;
            this.pf5[1] = (columDig - 1) + pivotX;

            pivotX = pivotX + columDig + espacio;

            int numero = Integer.parseInt(String.valueOf(digito));
            adicionarDigito(numero);
        }

        // Imprime matriz
        for (int i = 0; i < filasDig; i++) {
            for (int j = 0; j < totalColum; j++) {
                System.out.print(this.matrizImpr[i][j]);
            }
            System.out.println();
        }
    }

    /**
     *
     * Metodo encargado de procesar la entrada que contiene el size del segmento
     * de los digitos y los digitos a imprimir
     *
     * @param comando Entrada que contiene el size del segmento de los digito y
     * el numero a imprimir
     * @param espacioDig Espacio Entre digitos
     */
    public void procesar(String comando, int espacioDig) {
        if (!comando.contains(",")) {
            throw new IllegalArgumentException("Cadena " + comando
                    + " no contiene caracter ,");
        }
        //Se hace el split de la cadena
        String[] parametros = comando.split(",");
        //Valida la cantidad de parametros
        if (parametros.length > 2 || parametros.length < 2) {
            throw new IllegalArgumentException("Cadena " + comando
                    + " no contiene los parametros requeridos");
        }
        //Valida que el parametro size sea un numerico
        if (isNumeric(parametros[0])) {
            int tam = Integer.parseInt(parametros[0]);
            // se valida que el size este entre 1 y 10
            if (tam < 1 || tam > 10) {
                throw new IllegalArgumentException("El parametro size [" + tam
                        + "] debe estar entre 1 y 10");
            } else {
                // Realiza la impresion del numero
                imprimirNumero(tam, parametros[1], espacioDig);
            }
        } else {
            throw new IllegalArgumentException("Parametro Size [" + parametros[0]
                    + "] no es un numero");
        }
    }

    /**
     *
     * Metodo encargado de validar si una cadena es numerica
     *
     * @param cadena Cadena
     * @return true si es bumerico, de lo contrario false
     */
    public static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }
}
